package com.lht.synthesize.service;

import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.entitybase.response.CommonQueryPageResponse;
import com.lht.synthesize.model.SmsLog;

import java.util.List;

/**
 * 短信发送日志服务接口
 */
public interface SmsLogService {

    /**
     * 分页查询短信日志，返回QueryPageResponse对象
     * @return
     */
    CommonQueryPageResponse<List<SmsLog>> querySmsLogs(CommonQueryPageRequest<SmsLog> querySmsLogsRequest);

}
