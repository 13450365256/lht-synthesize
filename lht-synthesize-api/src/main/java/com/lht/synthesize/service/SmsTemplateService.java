package com.lht.synthesize.service;

import com.luckwine.parent.entitybase.request.CommonRequest;
import com.luckwine.parent.entitybase.response.CommonResponse;
import com.lht.synthesize.model.SmsTemplate;

/**
 * 短信模板服务接口
 */
public interface SmsTemplateService {

    /**
     * 短信模板详情
     *
     * @param smsTemplateDetailRequest 短信模板Id
     * @return
     */
    CommonResponse<SmsTemplate> smsTemplateDetail(CommonRequest<SmsTemplate> smsTemplateDetailRequest);

}
