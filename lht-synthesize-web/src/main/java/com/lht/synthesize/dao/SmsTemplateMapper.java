package com.lht.synthesize.dao;

import com.lht.synthesize.model.SmsTemplate;
import tk.mybatis.mapper.common.Mapper;

public interface SmsTemplateMapper extends Mapper<SmsTemplate> {

}