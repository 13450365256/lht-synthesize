package com.lht.synthesize.dao;

import com.lht.synthesize.model.SmsLog;
import tk.mybatis.mapper.common.Mapper;

public interface SmsLogMapper extends Mapper<SmsLog> {

}