package com.lht.synthesize.controller;

import com.lht.synthesize.handle.local.InsertUpdateSmsLogService;
import com.lht.synthesize.handle.local.QuerySmsLogsPageService;
import com.luckwine.marketing.model.MarketingExpenses;
import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.entitybase.response.CommonQueryPageResponse;
import com.lht.synthesize.handle.remote.QueryMarketingExpensesRemoteService;
import com.lht.synthesize.model.SmsLog;
import com.lht.synthesize.service.SmsLogService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@RestController
public class TestController {

    @Resource
    private SmsLogService smsLogService;

    @Resource
    private InsertUpdateSmsLogService insertUpdateSmsLogService;

    @Resource
    private QueryMarketingExpensesRemoteService queryMarketingExpensesRemoteService;

    @Resource
    private QuerySmsLogsPageService querySmsLogsPageService;

    @RequestMapping(value = "/local",method = RequestMethod.GET)
    public CommonQueryPageResponse<List<SmsLog>> testLocal(HttpServletResponse response) {
        CommonQueryPageRequest<SmsLog> querySmsLogsPageRequest = new CommonQueryPageRequest<SmsLog>();
        SmsLog smsLog = new SmsLog();
        smsLog.setMobile("13311111111");
        querySmsLogsPageRequest.setRequest(smsLog);
        querySmsLogsPageRequest.setPageSize(1);
        querySmsLogsPageRequest.setPageNo(1);
        return smsLogService.querySmsLogs(querySmsLogsPageRequest);
    }

    @RequestMapping(value = "/remote",method = RequestMethod.GET)
    public CommonQueryPageResponse<List<MarketingExpenses>> testRemote(HttpServletResponse response) {

        CommonQueryPageRequest<MarketingExpenses> CommonQueryPageRequest = new CommonQueryPageRequest<MarketingExpenses>();
        MarketingExpenses marketingExpenses = new MarketingExpenses();
        CommonQueryPageRequest.setPageNo(1);
        CommonQueryPageRequest.setPageSize(1);

        return queryMarketingExpensesRemoteService.call(CommonQueryPageRequest);

    }



    @RequestMapping(value = "/insert",method = RequestMethod.GET)
    public void insert() {
        for(int i=100;i<=200;i++){
            Date today = new Date();
            SmsLog smsLog = new SmsLog();
            smsLog.setContent("content:" + i);
            smsLog.setCreateTime(today);
            smsLog.setId(i + "");
            smsLog.setMobile("123456" + i);
            smsLog.setResultCode("" + i);
            smsLog.setResultMsg("abc");
            smsLog.setSmsId("a");
            smsLog.setUpdateTime(today);

            insertUpdateSmsLogService.insertSmsLog(smsLog);
        }

    }


    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public CommonQueryPageResponse<List<SmsLog>> query(HttpServletResponse response) {

        CommonQueryPageRequest<SmsLog> CommonQueryPageRequest = new CommonQueryPageRequest<SmsLog>();
        CommonQueryPageRequest.setPageNo(1);
        CommonQueryPageRequest.setPageSize(10);
        return querySmsLogsPageService.call(CommonQueryPageRequest);

    }
}
