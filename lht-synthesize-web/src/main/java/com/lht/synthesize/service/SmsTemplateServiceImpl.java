package com.lht.synthesize.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.luckwine.parent.entitybase.request.CommonRequest;
import com.luckwine.parent.entitybase.response.CommonResponse;
import com.lht.synthesize.handle.local.SmsTemplateDetailService;
import com.lht.synthesize.model.SmsTemplate;
import com.lht.synthesize.service.SmsTemplateService;

import javax.annotation.Resource;

@Service(validation = "true")
public class SmsTemplateServiceImpl implements SmsTemplateService {

    @Resource
    private SmsTemplateDetailService smsTemplateDetailService;

    @Override
    public CommonResponse<SmsTemplate> smsTemplateDetail(CommonRequest<SmsTemplate> smsTemplateDetailRequest) {
        return smsTemplateDetailService.call(smsTemplateDetailRequest);
    }
}
