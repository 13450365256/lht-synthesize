package com.lht.synthesize.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.entitybase.response.CommonQueryPageResponse;
import com.lht.synthesize.handle.local.QuerySmsLogsPageService;
import com.lht.synthesize.model.SmsLog;
import com.lht.synthesize.service.SmsLogService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 短信发送日志服务接口的实现类
 * 类似于控制层，只做服务的转发，业务逻辑的处理，放在handle中
 *
 * validation = "true"，开启服务端参数校验(默认关闭)
 */
//@Service
@Service(validation = "true")
public class SmsLogServiceImpl implements SmsLogService {

    @Resource
    private QuerySmsLogsPageService querySmsLogsPageService;

    @Override
    public CommonQueryPageResponse<List<SmsLog>> querySmsLogs(CommonQueryPageRequest<SmsLog> querySmsLogsRequest) {
        return querySmsLogsPageService.call(querySmsLogsRequest);
    }
}
