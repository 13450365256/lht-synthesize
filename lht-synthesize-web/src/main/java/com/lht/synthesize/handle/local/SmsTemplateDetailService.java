package com.lht.synthesize.handle.local;

import com.luckwine.parent.entitybase.request.CommonRequest;
import com.luckwine.parent.template.SingleTemplate;
import com.lht.synthesize.dao.SmsTemplateMapper;
import com.lht.synthesize.model.SmsTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 简单查询业务实现类：查询短信模板详情
 */
@Service
public class SmsTemplateDetailService extends SingleTemplate<SmsTemplate, SmsTemplate> {

    @Resource
    private SmsTemplateMapper smsTemplateMapper;

    @Override
    protected SmsTemplate callInner(CommonRequest<SmsTemplate> request) throws Exception {

        SmsTemplate smsTemplate = request.getRequest();
        String smsId =  smsTemplate.getSmsId();
        return smsTemplateMapper.selectByPrimaryKey(smsId);

    }

}
