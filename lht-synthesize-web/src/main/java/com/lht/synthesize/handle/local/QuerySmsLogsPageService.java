package com.lht.synthesize.handle.local;

import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.template.QueryPageTemplate;
import com.lht.synthesize.dao.SmsLogMapper;
import com.lht.synthesize.model.SmsLog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 分页查询业务实现类：查询短信发送记录
 */
@Service
public class QuerySmsLogsPageService extends QueryPageTemplate<SmsLog,List<SmsLog>, SmsLog> {

    @Resource
    private SmsLogMapper smsLogMapper;

    @Override
    protected List<SmsLog> callInner(CommonQueryPageRequest<SmsLog> request) throws Exception {
        return smsLogMapper.selectAll();
    }

    @Override
    protected List<SmsLog> transformationResponse(List<SmsLog> response, CommonQueryPageRequest<SmsLog> request) throws Exception {
        return response;
    }
}
