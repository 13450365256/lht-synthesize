package com.lht.synthesize.handle.remote;

import com.alibaba.dubbo.config.annotation.Reference;
import com.luckwine.marketing.model.MarketingExpenses;
import com.luckwine.marketing.service.MarketingExpensesService;
import com.luckwine.parent.entitybase.request.CommonQueryPageRequest;
import com.luckwine.parent.entitybase.response.CommonQueryPageResponse;
import com.luckwine.parent.template.QueryPageRemoteTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QueryMarketingExpensesRemoteService extends QueryPageRemoteTemplate<MarketingExpenses,List<MarketingExpenses>> {

    @Reference
    private MarketingExpensesService marketingExpensesService;

    @Override
    protected CommonQueryPageResponse<List<MarketingExpenses>> callRemote(CommonQueryPageRequest<MarketingExpenses> request) throws Exception {
        return marketingExpensesService.queryMarketingExpenses(request);
    }
}
