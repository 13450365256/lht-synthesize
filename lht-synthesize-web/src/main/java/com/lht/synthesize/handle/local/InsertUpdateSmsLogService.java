package com.lht.synthesize.handle.local;

import com.lht.synthesize.dao.SmsLogMapper;

import com.lht.synthesize.model.SmsLog;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class InsertUpdateSmsLogService {

    @Resource
    private SmsLogMapper smsLogMapper;

    public int insertSmsLog(SmsLog smsLog){
        return smsLogMapper.insert(smsLog);
    }


}
